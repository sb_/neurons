"""
Setup for complexity package.
"""

from setuptools import setup, find_packages
import codecs
import os

import neurons

HERE = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the relevant file


with codecs.open('DESCRIPTION.rst', encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()


setup(
    name='neurons',

    version=neurons.__version__,

    description='Classes related to Think Compplexity by Allen B. Downey',
    long_description=LONG_DESCRIPTION,

    url='TODO',

    author='Stefan Braun',
    author_email='sb@action.ms',

    license='MIT',

    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: science',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],

    keywords='science dev',

    packages=find_packages(),

    install_requires=['matplotlib'],

    # Create folders in package.
    # Copy files into this folder
    package_data={
        'neurons.test': ['test/.*'],
        'neurons.doc': ['doc/.*'],
    }
)
