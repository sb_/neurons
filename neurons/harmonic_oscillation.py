'''Harmonic oscillation of idealized spring
Created on 24.07.2014

:author: sb
'''

import matplotlib.pyplot as plt


def main():
    """Simple simulation of loaded spring."""
    duration = 10                # duration of simulation in s.
    delta_t = 0.01               # time interval for simulation in s.
    spring_c = -5.0              # spring constant
    init_pos = 5.0               # initial amplitude of the spring

    acc = [init_pos * spring_c]  # acceleration for each step
    vel = [0.0]                  # velocity for each step.
    pos = [init_pos]             # list of position per step
    time_ = [0.0]                # accumulated time for each step

    steps = int(duration / delta_t)
    for step in range(1, steps):
        acc.append(pos[-1] * spring_c)
        vel.append(vel[-1] + acc[-1] * delta_t)
        pos.append(pos[-1] + vel[-1] * delta_t)
        time_.append(delta_t * step)
    plt.plot(time_, pos, label='position')
    plt.plot(time_, vel, label='velocity')
    plt.plot(time_, acc, label='acceleration')
    plt.title('Ideal spring and mass system.')
    plt.legend()
    plt.xlabel('time')
    plt.ylabel('pos / vel / acc')
    plt.grid(True, 'major', 'both')
    plt.show()
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main())
