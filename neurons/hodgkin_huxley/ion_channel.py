'''Model of ion channel.

Created on 03.08.2014

@author: sb
'''
import abc
import math


class IonChannel(object):

    '''Model of ion channel.'''
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def current(self, voltage):
        '''Calculate the current through the ion channel.

        :param numeric voltage: membrane potential.

        :return: current
        '''
        raise NotImplementedError('current() is an abstract method.')

    @abc.abstractmethod
    def get_data(self):
        '''Return the data created as list.'''
        raise NotImplementedError('get_data() is an abstract method.')


class NaChannel(IonChannel):

    '''Na ion channel.'''

    def __init__(self, voltage=0, delta_t=0.01):
        '''Initialize Na channel.

        :param numeric voltage: initial voltage.
        :param numeric delta_t: duration of a time step.
        '''
        self.__delta_t = delta_t
        # voltage in mV
        self.__reversal_voltage = 115
        # conductance in mS/cm^2
        self.__conductance = 120
        # initial potential
        self.__voltage = voltage
        self.__m, self.__h = self.__initialize_mh()
        self.__data = []

    def current(self, voltage):
        '''Current based on voltage.'''
        self.__next_m(voltage)
        self.__next_h(voltage)
        v_diff = voltage - self.__reversal_voltage
        i_na = self.__conductance * (self.__m ** 3) * self.__h * v_diff
        self.__data.append(i_na)
        return i_na

    def get_data(self):
        return self.__data

    def __initialize_mh(self):
        '''Initialize m and h.'''
        # initial m
        alpha_m_0 = self.__alpha_m(self.__voltage)
        beta_m_0 = self.__beta_m(self.__voltage)
        m_0 = alpha_m_0 / (alpha_m_0 + beta_m_0)
        # initial h
        alpha_h_0 = self.__alpha_h(self.__voltage)
        beta_h_0 = self.__beta_h(self.__voltage)
        h_0 = alpha_h_0 / (alpha_h_0 + beta_h_0)
        return (m_0, h_0)

    def __next_m(self, voltage):
        self.__m += self.__delta_m(voltage) * self.__delta_t

    def __next_h(self, voltage):
        self.__h += self.__delta_h(voltage) * self.__delta_t

    def __delta_m(self, voltage):
        d_m = (self.__alpha_m(voltage) * (1 - self.__m)) - \
            (self.__beta_m(voltage) * self.__m)
        return d_m

    def __delta_h(self, voltage):
        d_h = (self.__alpha_h(voltage) * (1 - self.__h)) - \
            (self.__beta_h(voltage) * self.__h)
        return d_h

    def __alpha_m(self, voltage):
        alpha = 2.5 - 0.1 * voltage
        alpha_m = alpha / (math.exp(alpha) - 1)
        return alpha_m

    def __beta_m(self, voltage):
        beta_m = 4 * math.exp((- 1) * voltage / 18)
        return beta_m

    def __alpha_h(self, voltage):
        alpha_h = 0.07 * math.exp((- 1) * voltage / 20)
        return alpha_h

    def __beta_h(self, voltage):
        beta_h = 1 / (math.exp(3 - (0.1) * voltage) + 1)
        return beta_h


class KChannel(IonChannel):
    '''K ion channel.'''

    def __init__(self, voltage=0, delta_t=0.01):
        '''Initialize K channel.

        :param numeric voltage: initial voltage.
        :param numeric delta_t: duration of a time step.
        '''
        self.__delta_t = delta_t
        # voltage in mV
        self.__reversal_voltage = -12
        # conductance in mS/cm^2
        self.__conductance = 36
        # initial potential
        self.__voltage = voltage
        self.__n = self.__initialize_n()
        self.__data = []

    def current(self, voltage):
        '''Current based on voltage.'''
        self.__next_n(voltage)
        v_diff = voltage - self.__reversal_voltage
        i_k = self.__conductance * (self.__n ** 4) * v_diff
        self.__data.append(i_k)
        return i_k

    def get_data(self):
        return self.__data

    def __initialize_n(self):
        '''Initialize n.'''
        # initial m
        alpha_n_0 = self.__alpha_n(self.__voltage)
        beta_n_0 = self.__beta_n(self.__voltage)
        n_0 = alpha_n_0 / (alpha_n_0 + beta_n_0)
        return n_0

    def __next_n(self, voltage):
        self.__n += self.__delta_n(voltage) * self.__delta_t

    def __delta_n(self, voltage):
        d_n = (self.__alpha_n(voltage) * (1 - self.__n)) - \
            (self.__beta_n(voltage) * self.__n)
        return d_n

    def __alpha_n(self, voltage):
        alpha = 0.1 - 0.01 * voltage
        alpha_n = alpha / (math.exp(1 - (0.1) * voltage) - 1)
        return alpha_n

    def __beta_n(self, voltage):
        beta_n = 0.125 * math.exp((-1) * voltage / 80)
        return beta_n


class LeakChannel(IonChannel):
    '''Channel modeling leak ion currents.'''

    def __init__(self, voltage=0, delta_t=0.01):
        '''Initialize leak channel.

        :param numeric voltage: initial voltage.
        :param numeric delta_t: duration of a time step.
        '''
        self.__delta_t = delta_t
        # voltage in mV
        self.__reversal_voltage = 10.6
        # conductance in mS/cm^2
        self.__conductance = 0.3
        # initial potential
        self.__voltage = voltage
        self.__data = []

    def current(self, voltage):
        '''Current based on voltage.'''
        v_diff = voltage - self.__reversal_voltage
        i_leak = self.__conductance * v_diff
        self.__data.append(i_leak)
        return i_leak

    def get_data(self):
        return self.__data
