'''Implementation of Hodgkin-Huxley neuron model.

Created on 03.08.2014

@author: sb
'''
from .ion_channel import NaChannel, KChannel, LeakChannel


class Neuron(object):
    '''Hodgkin-Huxley model of a neuron.
    '''

    def __init__(self, curr_fun, delta_t=0.01,
                 initial_voltage=0, capacitance=1):
        '''Configure simulation.'''
        self.curr_fun = curr_fun
        self.delta_t = delta_t
        self.__capacitance = capacitance
        self.__voltage = initial_voltage
        self.na_channel = NaChannel(voltage=self.__voltage,
                                    delta_t=self.delta_t)
        self.k_channel = KChannel(voltage=self.__voltage,
                                  delta_t=self.delta_t)
        self.leak_channel = LeakChannel(voltage=self.__voltage,
                                        delta_t=self.delta_t)

    def __potential(self, i_inject):
        '''Determine the membrane potential.'''
        i_na = self.na_channel.current(self.__voltage)
        i_leak = self.leak_channel.current(self.__voltage)
        i_k = self.k_channel.current(self.__voltage)
        current = i_inject - (i_na + i_k + i_leak)
        self.__voltage += (current / self.__capacitance) * self.delta_t

    def run(self):
        '''Run simulation.'''
        t_data = []
        v_data = []
        i_data = []
        time_ = 0
        for i_inject in self.curr_fun():
            i_data.append(i_inject)
            t_data.append(time_)
            time_ += self.delta_t
            v_data.append(self.__voltage)
            self.__potential(i_inject)
        return (t_data, v_data, i_data)

    def get_currents(self):
        '''Return (i_na, i_k, i_leak).'''
        return self.na_channel.get_data(), \
            self.k_channel.get_data(), \
            self.leak_channel.get_data()
