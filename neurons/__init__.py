"""Package for classes and functions around computational neurology.

(c)2014 Stefan Braun
"""

__version__ = '0.1.0dev'
