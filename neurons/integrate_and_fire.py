'''Implementation of the Integrate and Fire neuron model.

Created on 29.07.2014

@author: sb

'''
import matplotlib.pyplot as plt

RESISTANCE = 0.5
CAPACITY = 1
TAU = RESISTANCE * CAPACITY
DELTA_T = 0.005
THRESHOLD = 4


def preset_current():
    '''Create the current pulse.

    :returns: list of current values.
    '''
    current_ = []
    for _ in range(0, 80):
        current_.append(0)
    for _ in range(80, 550):
        current_.append(9)
    for _ in range(550, 750):
        current_.append(0)
    return current_


def calculate_voltage(resistance, tau, delta_t, threshold):
    '''Calculate the voltage response to the trigger.

    :param numeric resistance: value of model's resistor.
    :oaram numeric tau: time constant (R*C).
    :param numeric delta_t: duration of time steps.
    :param numeric threshold: voltage that triggers action potential.

    :return: (time-vector, voltage-vector)
    '''
    voltage = 0
    t_data = []
    v_data = []
    time_ = 0
    current = preset_current()
    data_points = len(current)
    # Calculate the resulting voltage
    for j in range(0, data_points):
        dvdt = (1 / tau) * (resistance * current[j] - voltage)
        voltage += dvdt * delta_t
        if voltage > threshold:
            v_data[-1] += 1
            voltage = 0
        time_ += delta_t
        t_data.append(time_)
        v_data.append(voltage)
    return (t_data, v_data)


def plot_voltage_over_time(t_data, v_data):
    '''Plot voltage over time.

    :param list t_data: time vector.
    :param list v_data: voltage vector.
    '''
    plt.plot(t_data, v_data)
    plt.axis([t_data[0], t_data[-1], -1, 10])
    plt.xlabel('Time')
    plt.ylabel('Voltage')
    plt.show()


def main():
    '''Plot voltage response over time.'''
    t_data, v_data = calculate_voltage(RESISTANCE, TAU, DELTA_T, THRESHOLD)
    plot_voltage_over_time(t_data, v_data)

if __name__ == '__main__':
    import sys
    sys.exit(main())
