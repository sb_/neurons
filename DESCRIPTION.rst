# Neuron Models

Simple spiking neuron models for simulation.

Currently *Integrate And Fire* and *Hodgkin-Huxley* models are available.
